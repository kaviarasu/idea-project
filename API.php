<?php
include_once("HomeController.php");
//DEFINE YOUR API on the putData function from HomeController.php. This API should be called from home.js under the JS folder.

header('Content-Type: application/json');
$aResult = array();
echo $_POST['state'];
$aResult['state'] = $_POST['state'];
if( !isset($_POST['state']) ) { $aResult['error'] = 'State name is empty'; }

if( !isset($aResult['error']) ) {
    $aResult['result'] = putData($_POST['state'], $_POST['count']);
}

echo json_encode($aResult);
?>
