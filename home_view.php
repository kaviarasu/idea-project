<!doctype html>
<html lang="en">
<head>
    <!-- External Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

    <!-- Own Styles -->
    <link rel="stylesheet" href="css/custom.css">
</head>
<body>
    <div id="loader" class="center"></div>
    <div id="flexContainer">
        <div class="MainContainer">
            <div style="position: relative; margin: 1rem;">
                <div class="Level" style="">
                    <div class="level-item is-confirmed fadeInUp" style="animation-delay: 750ms;">
                        <h5>Confirmed</h5>
                        <h4 id="cIncrease">-</h4>
                        <h1 id="cCount">-</h1>
                    </div>
                    <div class="level-item is-active fadeInUp" style="animation-delay: 1000ms;">
                        <h5>Active</h5>
                        <h4>&nbsp;</h4>
                        <h1 id="aCount">-</h1>
                    </div>
                    <div class="level-item is-recovered fadeInUp" style="animation-delay: 1250ms;">
                        <h5>Recovered</h5>
                        <h4 id="rIncrease">-</h4>
                        <h1 id="rCount">-</h1>
                    </div>
                    <div class="level-item is-deceased fadeInUp" style="animation-delay: 1500ms;">
                        <h5>Deceased</h5>
                        <h4 id="dIncrease">-</h4>
                        <h1 id="dCount">-</h1>
                    </div>
                </div>
            </div>

            <table id="statsTable" class="display">
            <thead>
                <tr>
                    <th data-orderable="false">Date</th>
                    <th>Confirmed</th>
                    <th>Recovered</th>
                    <th>Death</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="statsTableBody">
            </tbody>
            </table>
        </div>
        <div class="FilterContainer">
            <h5>Filter</h5>
            <div class="col-7 col-md-5">
                <label>State</label>
                <select class="selectpicker" id="selectpicker" data-live-search="true">
                </select>
            </div>
            <div class="col-5 col-md-5" style="margin-top: 1rem;">
                <label>Date range</label>
                <input class="datepicker" name="daterange">
            </div>
        </div>
    </div>

    <!-- External Script files -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

    <!-- Own Scripts -->
    <script src="js/home.js"></script>
</body>
</html>
