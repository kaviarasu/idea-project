//WRITE YOU JS CODE
//ANY APIS should be called in this file.
//Manipulate html from this file including showing data. Your base html rests in home_view.php

var currentState = "AK";
var showLoader = true;

var dateRangerCallback = function(start, end, label) {
    startDate = start;
    endDate = end;
    $('input[name="daterange"] span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    getStateStats();
};

document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
        showLoader();
    } else {
        hideLoader();
    }
};

var showLoader = function() {
    document.querySelector(".MainContainer").style.visibility = "hidden";
    document.querySelector("#loader").style.visibility = "visible";
    document.querySelector("#loader").style.display = "block";
}

var hideLoader = function() {
    document.querySelector("#loader").style.display = "none";
    document.querySelector(".MainContainer").style.visibility = "visible";
}

$(document).ready(function () {
    initUSAStates();
    $('input[name="daterange"]').daterangepicker({
        "opens": 'down',
        "minDate": "03/05/2020",
        "maxDate": moment().format("MM/DD/YYYY"),
        "ranges": {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, dateRangerCallback);

    // $.fn.dataTable.moment('ddd, MMMM Do YYYY');
    $('#statsTable').DataTable({
        "order": [],
        "columns": [
            { "data": "date"},
            { "data": "confirmed", render: $.fn.dataTable.render.number(',', '') },
            { "data": "recovered", render: $.fn.dataTable.render.number(',', '') },
            { "data": "death", render: $.fn.dataTable.render.number(',', '') },
            { "data": "total", render: $.fn.dataTable.render.number(',', '') }
        ],
    });
    getStateStats();
});


var initUSAStates = function() {
    var that = this;
    showLoader();
    const request = $.ajax({
        url: "https://api.covidtracking.com/v1/states/info.json",
    });

    request.done(function(response) {
        var fragment = document.createDocumentFragment();
        response.forEach(function(s) {
             var option = document.createElement('option');
             option.textContent = s.name;
             $(option).attr("data-tokens", s.state + " " + s.name);
             $(option).attr("value", s.state);
             fragment.append(option);
             $("#selectpicker").append($(fragment));
        });

        $("#selectpicker").selectpicker('refresh');

        $('#selectpicker').on('change', function(e){
            currentState = this.value;
            getStateStats();
        });
    }).fail(function(jqXHR, textStatus, errorThrown) {
    });
}

var getStateStats = function() {
    showLoader();
    var drp = $('input[name="daterange"]').data('daterangepicker');
    var startTime = drp.startDate;
    var endTime = drp.endDate;
    const dateRange = getDates(startTime, endTime);
    const request = $.ajax({
        url: "https://api.covidtracking.com/v1/states/" + currentState.toLowerCase() + "/daily.json",
    });

    request.done(function(response) {
        processAPIResponse(dateRange, response);
        hideLoader();
    }).fail(function(jqXHR, textStatus, errorThrown) {

    });
}

var processAPIResponse = function(dateRangeArr, results) {
    var statsReport = [];
    results.forEach(function(stat) {
        var momentDate = moment(stat.date, "YYYYMMDD");
        if(dateRangeArr.includes(momentDate.format("YYYYMMDD"))) {
            statsReport.push(stat);
        }
    });
    var finalData = [];
    var cIncrease = 0;
    var rIncrease = 0;
    var dIncrease = 0;
    statsReport.forEach(function (currentStat) {
        var data = {};
        data["date"] = moment(currentStat.date, "YYYYMMDD").format("Do MMMM YYYY");
        cIncrease += currentStat.positiveIncrease;
        rIncrease += currentStat.negativeIncrease;
        dIncrease += currentStat.deathIncrease;
        data["confirmed"] = currentStat.positiveIncrease;
        data["recovered"] = currentStat.negativeIncrease;
        data["death"] = currentStat.death;
        data["total"] = currentStat.total;
        finalData.push(new DayStat(data.date, data.confirmed, data.recovered, data.death, data.total));
    });

    $("#cIncrease").text("+" + formatNumber(cIncrease));
    $("#rIncrease").text("+" + formatNumber(rIncrease));
    $("#dIncrease").text("+" + formatNumber(dIncrease));

    if(statsReport != null && statsReport[0] != undefined) {
        var cCount = statsReport[0].positive;
        var aCount = statsReport[0].positive;
        var rCount = statsReport[0].negative;
        var dCount = statsReport[0].death;
        $("#cCount").text(formatNumber(cCount));
        $("#aCount").text(formatNumber(aCount));
        $("#rCount").text(formatNumber(rCount));
        $("#dCount").text(formatNumber(dCount));
    }

    reInitDataTable(finalData);
    updateDB(currentState, statsReport != null ? statsReport.length : 0);
}

var updateDB = function(state, numberofresults) {
    const request = $.ajax({
        type: "POST",
        url: 'API.php',
        dataType: 'json',
        data: {'state': state, 'count': numberofresults}
    });

    request.done(function(response) {
    }).fail(function(jqXHR, textStatus, errorThrown) {

    });
}

var reInitDataTable = function(data) {
    var datatable = $('#statsTable').DataTable();
    datatable.clear();
    datatable.rows.add(data);
    datatable.draw();
}


var getDates = function(startMoment, endMoment) {
    var arr = [];
    var start = startMoment.toDate();
    var end = endMoment.toDate();
    var currentDate = start;
    while (currentDate <= end) {
        arr.push(moment(currentDate).format("YYYYMMDD"));
        currentDate = moment(currentDate).add(1, 'days');
    }
    return arr;
}

var formatNumber = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function DayStat(date, confirmed, recovered, death, total) {
    this.date = date;
    this.confirmed = confirmed;
    this.recovered = recovered;
    this.death = death;
    this.total = total;
};
