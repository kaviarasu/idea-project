First, Datacase setup. 

1. Login to mysql console as a root user, `mysql -u root -p`
2. Create a db called, "ub". Switch db using, `use ub;`
3. Create corresping table for storing the results. 
    
    `CREATE TABLE MyResults (
    -> id INT AUTO_INCREMENT PRIMARY KEY,
    -> timestamp TIMESTAMP NOT NULL
    -> statename VARCHAR(3) NOT NULL,
    -> numberofresults LONGINT NOT NULL
    -> );`


4. Switch to root folder of the project and run 
    `php -S localhost:8000`
    
